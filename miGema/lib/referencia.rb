#!/usr/bin/ruby
# encoding: utf-8

class Referencia
	private
    attr_accessor :authors, :title, :serie, :editorial, :edition, :date, :isbn
    
    public 
    # CONSTRUCTOR
	def initialize    title, serie, editorial, edition, date, (*authors), (*isbn)
        @title = title
        @serie = serie
        @editorial = editorial
        @edition = edition
        @date = date
        self.authors = []
        authors.each do |palabras|
        	@authors.push(palabras)
        	#puts palabras
        end
        self.isbn = []
        isbn.each do |palabras|
        	@isbn.push(palabras)
        	#puts palabras
        end
        return self
	end
	
	
	def get_authors
	    @authors
	end
	
	def get_title
	    @title
	end
	
	def get_serie
		@serie
	end
	
	def get_editorial
		@editorial
	end
	
	def get_edition
		@edition
	end
	
	def get_date
		@date
	end
	
	def get_isbn
		@isbn
	end
end