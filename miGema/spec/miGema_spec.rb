require 'spec_helper'

# ============ SINTAXIS =======================
#describe "# Breve descripcion del grupo de expectativas...agrupación de varias expectativas" do
#   it "Debe existir uno o más autores" do
#     @miGema.get_autores.length.should_not be 0
#   end
#end
# ============================================

describe MiGema do
  # Antes de cada prueba hacer:
  before :each do
    @ref1 = Referencia.new("titulo","","Santana", 4, "07/07/2005", ["Jorge","pable"], [00])
  end
  
  
  #it 'Tiene un número de versión' do
  #  expect(MiGema::VERSION).not_to be nil
  #end

  #it 'Hace algo útil' do
  #  expect(false).to eq(false)
  #end
  
  # Debe existir uno o más autores
  it 'Debe existir uno o más autores' do
    expect(@ref1.get_authors.size).not_to be 0
  end

  # Debe existir un título
  it 'Debe existir un título' do
    expect(@ref1.get_title).not_to be_empty
  end
  
  # Debe existir o no una serie.
  it 'Debe existir o no una serie' do
    expect(@ref1.get_serie.length).to be >= 0
  end
  
  # Debe existir una editorial.
  it 'Debe existir una editorial' do
    expect(@ref1.get_editorial).not_to be_empty  
  end
  
  # Debe existir un número de edición.
  it 'Debe existir un número de edición' do
    expect(@ref1.get_edition).to be > 0  
  end
  
  # Debe existir una fecha de publicación.
  it 'Debe existir una fecha de publicación' do
    expect(@ref1.get_date).not_to be_empty 
  end
  
  # Debe existir uno o más números ISBN.
  it 'Debe existir uno o más números ISBN' do
    expect(@ref1.get_isbn.size).not_to be 0
  end
  
  # Existe un método para obtener el listado de autores.
  it 'Existe un método para obtener el listado de autores' do
    expect(@ref1).to respond_to(:get_authors) 
  end  
end








# Existe un método para obtener el título.
# Existe un método para obtener la serie.
# Existe un método para obtener la editorial.
# Existe un método para obtener el número de edición.
# Existe un método para obtener la fecha de publicación.
# Existe un método para obtener el listado de ISBN.
# Existe un método para obtener la referencia formateada.